import requests
from bs4 import BeautifulSoup
import re
import os
import sys


def fetch_teams():
    print("Fetching team page...", end=" ")
    sys.stdout.flush()

    url = 'http://hwo.azurewebsites.net/?sort=keimola'
    html = requests.get(url).text
    soup = BeautifulSoup(html)
    all_rows = soup.find_all('tr')

    race_names = []
    for td in all_rows[0].find_all('td')[2:-3]:
        race_names.append(td.text)

    teams = []
    for team in all_rows[1:]:
        links = team.find_all('a')
        o = {'name': links[0].text, 'races': [], 'url': links[0].get('href')}
        teams.append(o)

        i = 0
        for link in links[1:]:
            o['races'].append(
                {'name': race_names[i],
                 'time': link.text,
                 'id': link.get('href')[-24:],
                 'url': link.get('href')})
            i += 1

    print('Done! Fetched %s teams.' % len(teams))
    return teams, race_names


def fetch_race_urls(team, race_names):
    print('Fetching race list...', end=" ")
    sys.stdout.flush()

    html = requests.get(team['url']).text
    soup = BeautifulSoup(html)
    for name in race_names:
        div = soup.find('div', {'class': name.lower()})
        races = div.find_all('div', {'class': 'race'})
        for race in races:
            link = race.find('a')
            if link is None:
                continue

            race_id = link.get('href')[-24:]
            if any(r['id'] == race_id for r in team['races']):
                continue

            team['races'].append(
                {'name': name,
                 'time': float(race.get('data-result')) / 1000.0,
                 'id': race_id,
                 'url': 'https://helloworldopen.com' + link.get('href')})
    print('Done!')


def fetch_race_data(team):
    root_dir = 'scraped/'
    if not os.path.exists(root_dir):
        os.makedirs(root_dir)

    regex = re.compile('\?recording=(.+\.json)')
    for race in team['races']:
        print('Fetching %s@%s...' % (race['name'], race['time']), end=" ")
        sys.stdout.flush()

        path = '%s%s_%s_%s_%s.json' % (root_dir, team['name'], race['name'], race['time'], race['id'])
        path = path.replace(':', '.')

        if os.path.exists(path):
            print('Skip! %s@%s already fetched at %s.' % (race['name'], race['time'], path))
            continue

        #fetch data json url
        html = requests.get(race['url']).text
        result = regex.search(html)
        race['dataUrl'] = result.groups(1)[0]

        #fetch data json
        #urllib.request.urlretrieve(race['dataUrl'], path)
        headers = {'User-Agent': 'Chrome'}

        r = requests.get(race['dataUrl'], stream=True, headers=headers)
        with open(path, 'wb') as fd:
            for chunk in r.iter_content(1024):
                fd.write(chunk)
        print('Done! %s retrieved to %s.' % (race['dataUrl'], path))


def scrape():
    teams, race_names = fetch_teams()
    print()
    for team in teams:
        print('Fetching data for %s:' % (team['name']))
        fetch_race_urls(team, race_names)
        fetch_race_data(team)
        print()


scrape()