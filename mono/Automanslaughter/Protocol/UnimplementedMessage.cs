﻿using System;

namespace Automanslaughter.Protocol
{
    public class UnimplementedMessage : Message
    {
        private string _msgType;

        public UnimplementedMessage(string type)
        {
            _msgType = type;
        }

        public override string msgType
        {
            get { return _msgType; }
        }
    }
}