namespace Automanslaughter.Protocol.Outgoing
{
    public class Join : Message
    {
        public const string MsgType = "join";
        public override string msgType
        {
            get { return MsgType; }
        }

        public string Name { get; set; }

        public string Key { get; set; }
    }
}