namespace Automanslaughter.Protocol.Outgoing
{
    public class Ping : Message
    {
        public const string MsgType = "ping";
        public override string msgType
        {
            get { return MsgType; }
        }

        protected override object GetData()
        {
            return null;
        }
    }
}