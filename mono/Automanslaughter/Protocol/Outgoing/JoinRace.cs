﻿namespace Automanslaughter.Protocol.Outgoing
{
    public class JoinRace : Message
    {
        public const string MsgType = "joinRace";

        public override string msgType
        {
            get { return MsgType; }
        }

        public BotId BotId { get; set; }

        public string TrackName { get; set; }
        public int CarCount { get; set; }
    }

    public class BotId
    {
        public string Name { get; set; }
        public string Key { get; set; }
    }
}
