﻿using System;

namespace Automanslaughter.Protocol.Outgoing
{
    public class Turbo : Message
    {
        private static readonly Random Random = new Random();

        public const string MsgType = "turbo";

        public string TurboString { get; private set; }

        public Turbo()
        {
            TurboString = GetTurboString();
        }

        public override string msgType
        {
            get { return MsgType; }
        }

        protected override object GetData()
        {
            return TurboString;
        }

        private string GetTurboString()
        {
            // this is super important stuff
            var verbs = new[] {"Eat", "Taste", "Lick", "Have some", "Get", "Here's", "Suck"};
            var adjectives = new[] {"fiery", "hot", "burning", "melting", "nitrous", "poisonous", "lightning-fast"};
            var nouns = new[] {"napalm", "nitro", "turbo", "boost", "afterburner", "exhaust gas"};
            var insults = new[] {"scum", "slowpoke", "lazy ass", "snail", "weakling", "sunday driver", "grandma"};
            return string.Format("{0} {1} {2}, you {3}!", GetRandomElement(verbs), GetRandomElement(adjectives), GetRandomElement(nouns), GetRandomElement(insults));
        }

        private object GetRandomElement(string[] arr)
        {
            return arr[Random.Next(arr.Length)];
        }
    }
}
