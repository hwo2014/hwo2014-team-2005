using System;

namespace Automanslaughter.Protocol.Outgoing
{
    public class Throttle : Message
    {
        private double _value;
        public const string MsgType = "throttle";
        public override string msgType
        {
            get { return MsgType; }
        }

        public double Value
        {
            get { return _value; }
            set
            {
                if (value < 0) Console.WriteLine("WARNING! TRYING TO SET VALUE LESS THAN 0: {0}", value);
                if (value > 1) Console.WriteLine("WARNING! TRYING TO SET VALUE OVER 1: {0}", value);
                _value = value < 0 ? 0 : value > 1 ? 1 : value;
            }
        }

        protected override object GetData()
        {
            return Value;
        }
    }
}