using System;

namespace Automanslaughter.Protocol.Outgoing
{
    public class SwitchLane : Message
    {
        public SwitchLane()
        {

        }

        public SwitchLane(Turn turn)
        {
            if (turn == Turn.Left)
                Value = "Left";

            if (turn == Turn.Right)
                Value = "Right";

            if(turn == Turn.None)
                throw new InvalidOperationException("Cannot turn to nothing!");
        }

        public const string MsgType = "switchLane";
        public override string msgType
        {
            get { return MsgType; }
        }

        public string Value { get; set; }

        protected override object GetData()
        {
            return Value;
        }
    }
}