﻿using System;
using System.Collections.Generic;
using Automanslaughter.Protocol.Incoming;
using Automanslaughter.Protocol.Outgoing;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Automanslaughter.Protocol
{
    public class MessageJsonConverter : Newtonsoft.Json.Converters.CustomCreationConverter<Message>
    {
        public override Message Create(Type objectType)
        {
            throw new NotSupportedException();
        }

        public virtual Message Create(Type objectType, JObject jObject)
        {
            var type = (string)jObject.Property("msgType");
            switch (type)
            {
                case YourCar.MsgType:
                    return new YourCar();
                case CarPositions.MsgType:
                    return new CarPositions();
                case Join.MsgType:
                    return new Join();
                case GameInit.MsgType:
                    return new GameInit();
                case GameStart.MsgType:
                    return new GameStart();
                case JoinRace.MsgType:
                    return new JoinRace();
                case Turbo.MsgType:
                    return new Turbo();
                case TurboAvailable.MsgType:
                    return new TurboAvailable();
                case Crash.MsgType:
                    return new Crash();
                case TurboEnd.MsgType:
                    return new TurboEnd();
                case Spawn.MsgType:
                    return new Spawn();
            }

            return new UnimplementedMessage(type);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            // Load JObject from stream
            JObject jObject = JObject.Load(reader);

            // Create target object based on JObject
            var target = Create(objectType, jObject);

            var msgType = (string)jObject.Property("msgType");

            // Populate the object properties
            var data = jObject.SelectToken("data");
            if (data != null)
            {
                switch (msgType)
                {
                    case "gameStart":
                        return target;
                    case "carPositions":
                        var foo = serializer.Deserialize<List<CarPosition>>(data.CreateReader());
                        var gameTickNode = jObject.SelectToken("gameTick");
                        return new CarPositions
                        {
                            Positions = foo.ToArray(),
                            gameTick = gameTickNode == null ? null : (int?) gameTickNode.Value<int>()
                        };
                    default:
                        try
                        {
                            serializer.Populate(data.CreateReader(), target);
                        }
                        catch (JsonSerializationException)
                        {
                            Console.WriteLine("Unable to deserialize data for type {0}!", target.msgType);
                        }
                        break;
                }
            }

            return target;
        }
    }
}
