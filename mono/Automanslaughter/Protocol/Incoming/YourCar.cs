﻿namespace Automanslaughter.Protocol.Incoming
{
    public class YourCar : Message
    {
        public const string MsgType = "yourCar";
        public override string msgType
        {
            get { return MsgType; }
        }

        public string Name { get; set; }

        public string Color { get; set; }

    }
}
