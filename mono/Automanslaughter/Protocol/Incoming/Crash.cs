﻿namespace Automanslaughter.Protocol.Incoming
{
    public class Crash : Message
    {
        public const string MsgType = "crash";

        public override string msgType
        {
            get { return MsgType; }
        }

        public string Name { get; set; }
        public string Color { get; set; }
    }
}
