﻿namespace Automanslaughter.Protocol.Incoming
{
    public class TurboAvailable : Message
    {
        public const string MsgType = "turboAvailable";

        public override string msgType
        {
            get { return MsgType; }
        }

        public double TurboDurationMilliseconds { get; set; }

        public int TurboDurationTicks { get; set; }

        public double TurboFactor { get; set; }
    }
}
