﻿namespace Automanslaughter.Protocol.Incoming
{
    public class Spawn : Message
    {
        public const string MsgType = "spawn";

        public override string msgType
        {
            get { return MsgType; }
        }

        public string Name { get; set; }
        public string Color { get; set; }
    }
}
