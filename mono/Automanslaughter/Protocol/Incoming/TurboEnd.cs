﻿namespace Automanslaughter.Protocol.Incoming
{
    public class TurboEnd : Message
    {
        public const string MsgType = "turboEnd";

        public override string msgType
        {
            get { return MsgType; }
        }

        public string Name { get; set; }
    }
}