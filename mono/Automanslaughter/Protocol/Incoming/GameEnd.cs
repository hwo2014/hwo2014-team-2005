﻿namespace Automanslaughter.Protocol.Incoming
{
    public class GameEnd : Message
    {
        public const string MsgType = "gameEnd";
        public override string msgType
        {
            get { return MsgType; }
        }
    }
}