﻿using System;
using System.Linq;

namespace Automanslaughter.Protocol.Incoming
{
    public class CarPositions : Message
    {
        public const string MsgType = "carPositions";
        public override string msgType
        {
            get { return MsgType; }
        }

        public CarPosition[] Positions { get; set; }

        public CarPosition CarPosition(string botName)
        {
            var ef = Positions.FirstOrDefault(m => m.Id.Name.Equals(botName, StringComparison.Ordinal));

            return ef;
        }
    }

    public class CarPosition
    {
        public CarId Id { get; set; }
        public virtual double Angle { get; set; }
        public PiecePosition PiecePosition { get; set; }
    }

    public class CarId
    {
        public string Name { get; set; }
        public string Color { get; set; }
    }

    public class Lane
    {
        public int StartLaneIndex { get; set; }
        public int EndLaneIndex { get; set; }
    }
    public class PiecePosition
    {
        public int PieceIndex { get; set; }
        public double InPieceDistance { get; set; }
        public Lane Lane { get; set; }
        public int Lap { get; set; }
    }
    public class Race
    {
        public Track Track { get; set; }

        public Car[] Cars { get; set; }
        public RaceSession RaceSession { get; set; }
    }

    public class RaceSession
    {
        public int Laps { get; set; }
        public int MaxLapTimeMs { get; set; }
        public bool QuickRace { get; set; }
        public int DurationMs { get; set; }
    }

    public class Car
    {
        public CarId Id { get; set; }
        public Dimensions Dimensions { get; set; }
    }

    public class Dimensions
    {
        public double Length { get; set; }
        public double Width { get; set; }
        public double GuideFlagPosition { get; set; }
    }

    public class Track
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public Piece[] Pieces { get; set; }
        public StartingPoint StartingPoint { get; set; }
        public TrackLane[] Lanes { get; set; }
    }

    public class TrackLane
    {
        public int Index { get; set; }
        public double DistanceFromCenter { get; set; }
    }

    public class Piece
    {
        public int Index { get; set; }
        public double Length { get; set; }
        public bool Switch { get; set; }
        public int Radius { get; set; }
        public double Angle { get; set; }

        public Turn Turn { get { return Math.Abs(Angle) < 0.01 ? Turn.None : Angle < 0 ? Turn.Left : Turn.Right; } }

        public double CalculatedLength
        {
            get
            {
                return Length > 0 ? Length : (Math.Abs(Angle) * Math.PI / 180) * Radius;
            }
        }

        public double Steepness
        {
            get { return Math.Sqrt(Math.Abs(Angle)/CalculatedLength); }
        }
    }

    public class GameInit : Message
    {
        public const string MsgType = "gameInit";
        public override string msgType
        {
            get { return MsgType; }
        }

        public Race Race { get; set; }
    }

    public class StartingPoint
    {
        public Position Position { get; set; }
        public double Angle { get; set; }
    }

    public class Position
    {
        public double X { get; set; }
        public double Y { get; set; }
    }
}
