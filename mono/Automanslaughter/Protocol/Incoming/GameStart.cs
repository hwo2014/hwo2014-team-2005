﻿namespace Automanslaughter.Protocol.Incoming
{
    public class GameStart : Message
    {
        public const string MsgType = "gameStart";
        public override string msgType
        {
            get { return MsgType; }
        }
    }
}
