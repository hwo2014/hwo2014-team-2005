using System.Dynamic;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Automanslaughter.Protocol
{
    public abstract class Message
    {
        private readonly JsonSerializerSettings _settings = new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            NullValueHandling = NullValueHandling.Ignore
        };

        [JsonIgnore]
        public abstract string msgType { get; }

        public int? gameTick { get; set; }

        public string ToJson()
        {
            var data = GetData();

            dynamic msg = new ExpandoObject();
            msg.msgType = msgType;

            if (data != null)
                msg.data = data;

            if (gameTick != null)
                msg.gameTick = gameTick;

            return JsonConvert.SerializeObject(msg, _settings);
        }

        protected virtual object GetData()
        {
            return this;
        }

        public static Message FromJson(string json)
        {
            return JsonConvert.DeserializeObject<Message>(json, new MessageJsonConverter());
        }
    }
}