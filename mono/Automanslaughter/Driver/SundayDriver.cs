using System;
using Automanslaughter.Protocol;
using Automanslaughter.Protocol.Incoming;
using Automanslaughter.Protocol.Outgoing;

namespace Automanslaughter.Driver
{
    public class SundayDriver : BaseDriver
    {
        private bool _turnRequestedCurrentPiece = false;
        private int _previousPosition = 0;

        public SundayDriver(Dashboard dashboard) : base(dashboard)
        {
        }

        public override string Name
        {
            get { return "SundayDriver"; }
        }

        protected override Message DriverMove(CarPositions positions)
        {
            var myPosition = positions.CarPosition(TeamName);
            var currentPosition = myPosition.PiecePosition.PieceIndex;

            if (_previousPosition != currentPosition)
            {
                _previousPosition = currentPosition;
                _turnRequestedCurrentPiece = false;
            }

            var turnInNextPiece = Dashboard.NextTurn(currentPosition);
            var currentPiece = Dashboard.GetPiece(currentPosition);

            if (turnInNextPiece == Turn.None || Messages.Peek().msgType != Throttle.MsgType)
            {
                // default throttle
                double throttle = 0.50;

                // slow down before corner
                var distanceToCorner = 100 - myPosition.PiecePosition.InPieceDistance;
                if (distanceToCorner < 34 && turnInNextPiece != Turn.None)
                {
                    throttle *= distanceToCorner / 100;
                }

                // deep corner speeds
                 if (Math.Abs(currentPiece.Angle) > 44)
                    throttle = 0.67;

                // drifting too much, slow down
                var driftAngle = Math.Abs(myPosition.Angle);
                if ( driftAngle > 42)
                {
                    throttle = (73 - driftAngle) / 100;

                    if (throttle < 0)
                        throttle = 0;
                }


                var message = new Throttle {Value = throttle};
                return message;
            }

            if (!_turnRequestedCurrentPiece && turnInNextPiece != Turn.None)
            {
                Console.WriteLine("changing lane {0}", turnInNextPiece);
                _turnRequestedCurrentPiece = true;
                return new SwitchLane(turnInNextPiece);
            }

            return new Ping();
        }
    }
}


