﻿using System;
using System.Linq;

namespace Automanslaughter.Driver
{
    class TinaTurner : ICoDriver
    {
        private const double RiskFactor = 55;

        public Dashboard Dashboard { get; private set; }

        public TinaTurner(Dashboard dashboard)
        {
            Dashboard = dashboard;
        }

        public double WhatsUp()
        {
            var myCarInfo = Dashboard.MyCarInfo;
            var angle = Math.Abs(myCarInfo.CarPosition.Angle);
            var angleVelocity = myCarInfo.CarAngleVelocity;
            var angleAcceleration = myCarInfo.CarAngleAcceleration;

            if (angleVelocity < 0) return 1;

            var projectedMaxAngle = angle;
            var projectedSpeed = myCarInfo.Velocity;
            var projectedPiece = Dashboard.MyCurrentPiece();
            var turn = projectedPiece.Turn;
            var projectedInPieceDistance = myCarInfo.CarPosition.PiecePosition.InPieceDistance;
            var projectedAngleAcceleration = angleAcceleration;
            for (int t = 1;; t++)
            {
                projectedMaxAngle += angleVelocity + (projectedAngleAcceleration * t);
                if (projectedMaxAngle > RiskFactor)
                {
                    Console.WriteLine("TURN - Angle too high {0}, angle change {1}, angle acceleration {2}, projected max angle {3}, breaking", angle.ToString("F3"), angleVelocity.ToString("F3"), angleAcceleration.ToString("F3"), projectedMaxAngle.ToString("F3"));
                    return 0;
                }
                projectedSpeed *= 0.98;
                projectedInPieceDistance += projectedSpeed;
                while (projectedInPieceDistance > projectedPiece.CalculatedLength)
                {
                    projectedInPieceDistance -= projectedPiece.CalculatedLength;
                    projectedPiece = Dashboard.NextPieces(projectedPiece.Index).First();
                    if (projectedPiece.Turn != turn) break;
                }
                if (projectedSpeed < 4.0 / Dashboard.MyCurrentPiece().Steepness) break;
            }

            return 1;
        }
    }
}