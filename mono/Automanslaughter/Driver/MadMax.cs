using Automanslaughter.Protocol;
using Automanslaughter.Protocol.Incoming;
using Automanslaughter.Protocol.Outgoing;

namespace Automanslaughter.Driver
{
    public class MadMax : BaseDriver
    {
        public MadMax(Dashboard dashboard) : base(dashboard)
        {
        }

        public override string Name
        {
            get
            {
                return "MadMax";
            }
        }

        protected override Message DriverMove(CarPositions positions)
        {
            return new Throttle { Value = 1 };
        }
    }
}