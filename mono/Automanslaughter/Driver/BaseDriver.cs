using System.Collections.Generic;
using Automanslaughter.Protocol;
using Automanslaughter.Protocol.Incoming;

namespace Automanslaughter.Driver
{
    public abstract class BaseDriver
    {
        protected readonly Dashboard Dashboard;


        protected Stack<Message> Messages;


        public abstract string Name { get; }

        public string Key { get; set; }

        public string TeamName { get; set; }


        protected BaseDriver(Dashboard dashboard)
        {
            Dashboard = dashboard;
            Messages = new Stack<Message>();
        }

        public Message MakeMove(CarPositions positions)
        {
            var message =  DriverMove(positions);
            Messages.Push(message);

            return message;
        }

        protected abstract Message DriverMove(CarPositions positions);

    }
}