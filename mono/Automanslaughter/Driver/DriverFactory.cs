namespace Automanslaughter.Driver
{
    public class DriverFactory
    {
        public BaseDriver Create(string driverName, string teamName, string key, Dashboard dashboard)
        {
            BaseDriver driver;
            switch (driverName)
            {
                case "MadMax":
                    driver = new MadMax(dashboard);
                    break;
                case "LaneWarrior":
                    driver = new LaneWarrior(dashboard);
                    break;
                case "SundayDriver":
                    driver = new SundayDriver(dashboard);
                    break;
                default:
                    driver = new LaneWarrior(dashboard);
                    break;
            }

            driver.Key = key;
            driver.TeamName = teamName;

            return driver;
        }
    }
}