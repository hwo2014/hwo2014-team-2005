using System;
using System.Linq;
using Automanslaughter.Protocol;
using Automanslaughter.Protocol.Incoming;

namespace Automanslaughter.Driver
{
    class ToughBrake : ICoDriver
    {
        public Dashboard Dashboard { get; private set; }

        private double _friction = 0.440;

        public ToughBrake(Dashboard dashboard)
        {
            Dashboard = dashboard;

            Dashboard.Crashed += (sender, args) =>
            {
                _friction *= 0.91;
            };
        }

        public double WhatsUp()
        {

            var targetSpeed = CalculateMaxAngleSpeed(false);


            var v = Dashboard.MyCarInfo.Velocity;
            var ticksNeededToBrakeToEntrySpeed = (50 * (v - targetSpeed)) / 49 * v;

            var nextCornerPiece = Dashboard.MyNextPieces().FirstOrDefault(m => m.Turn != Turn.None);

            double distanceToNextPiece = 0;

            if (nextCornerPiece != null)
            {
                distanceToNextPiece = Dashboard.DistanceToNextTurn();
            }

            var ticksToNextCorner = distanceToNextPiece / Dashboard.MyCarInfo.Velocity;

            if (ticksToNextCorner <= ticksNeededToBrakeToEntrySpeed)
            {
                if (ticksToNextCorner < ticksNeededToBrakeToEntrySpeed - Dashboard.MyCarInfo.Velocity)
                    Console.WriteLine("BREAK - CANNOT BREAK IN TIME FFFFFUUUUUuuu");

                Console.WriteLine("BREAK - Breaking before angle, targetVelocity: {0}, velocity: {1}", targetSpeed.ToString("F3"), Dashboard.MyCarInfo.Velocity.ToString("F3"));
                return 0;
            }

            // must maintain corner speeds also
            if (Dashboard.MyCurrentPiece().Turn != Turn.None)
            {
                var currentTargetSpeed = CalculateMaxAngleSpeed(true);

                if (currentTargetSpeed < Dashboard.MyCarInfo.Velocity + Dashboard.MyCarInfo.Acceleration)
                {
                    Console.WriteLine("BREAK - MAINTAINING target velocity: {0}, current velocity: {1}", currentTargetSpeed.ToString("F3"), Dashboard.MyCarInfo.Velocity.ToString("F3"));
                    return 0;
                }
            }

            return 1;
        }

        private double CalculateMaxAngleSpeed(bool useCurrent)
        {
            Piece piece = useCurrent ? Dashboard.MyCurrentPiece() : Dashboard.MyNextPieces().FirstOrDefault(m => m.Turn != Turn.None);

            if (piece != null)
            {
                var radius = piece.Radius + Dashboard.GetCarDistanceFromCenter();

                var maxSpeed = Math.Sqrt(_friction * radius);

                return maxSpeed;
            }

            // ITS A DRAG RACE!
            return double.PositiveInfinity;
        }
    }
}