﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Automanslaughter.Protocol;
using Automanslaughter.Protocol.Incoming;
using Automanslaughter.Protocol.Outgoing;

namespace Automanslaughter.Driver
{
    public class LaneWarrior : BaseDriver
    {
        private int _lastPieceIndex = -1;
        private int _turboStartIndex = -1;

        private List<ICoDriver> _coDrivers = new List<ICoDriver>();

        public LaneWarrior(Dashboard dashboard) : base(dashboard)
        {
            _coDrivers.Add(new ToughBrake(dashboard));
            _coDrivers.Add(new SonicHedgehog());
            _coDrivers.Add(new TinaTurner(dashboard));
        }

        public override string Name
        {
            get { return "LaneWarrior"; }
        }

        protected override Message DriverMove(CarPositions positions)
        {
            if (Dashboard.InTheDitch)
                return new Ping();

            if (_turboStartIndex == -1)
                _turboStartIndex = Dashboard.GetStartIndexOfLongestStreak();

            var myCarInfo = Dashboard.MyCarInfo;
            var piece = Dashboard.MyCurrentPiece();
            var pieceIndex = myCarInfo.CarPosition.PiecePosition.PieceIndex;

            var logPieceChanges = true;
            if (_lastPieceIndex != pieceIndex && logPieceChanges)
            {
                Console.WriteLine("Piece: {0}, Switch: {1}, Length: {2}, Angle: {3}, Radius: {4}, Turn: {5}, Steepness: {6}", pieceIndex, piece.Switch, piece.Length, piece.Angle, piece.Radius, piece.Turn, piece.Steepness.ToString("F4"));
                Console.WriteLine("Speed: {0}, Acceleration: {1}, Angle: {2}, Lane: {3}->{4}", myCarInfo.Velocity.ToString("F4"), myCarInfo.Acceleration.ToString("F4"), myCarInfo.CarPosition.Angle.ToString("F3"), myCarInfo.CarPosition.PiecePosition.Lane.StartLaneIndex, myCarInfo.CarPosition.PiecePosition.Lane.EndLaneIndex);
            }

            var message = ShortestLane(pieceIndex);

            _lastPieceIndex = pieceIndex;

            return message ?? (GetThrottle());
        }

        private Message GetThrottle()
        {
            var lowestSpeed = _coDrivers.Min(c => c.WhatsUp());

            Message message = null;
            if (Math.Abs(lowestSpeed - 1.0) < 0.001)
            {
                message = ShouldWeHitTurbo();
                if(message != null)
                    Console.WriteLine("---------------- GOOOOOOOO LANEEEEEEEEEEEEEE WAAAAAAAAARRIOOOOOOOOOOOOOOOOOOR------------------");
            }

            return message ?? new Throttle {Value = lowestSpeed};
        }

        private Message ShortestLane(int pieceIndex)
        {
            if (_lastPieceIndex == pieceIndex) return null;

            if (!Dashboard.MyNextPieces().First().Switch) return null;

            var nextTurn = Turn.None;
            var pi = pieceIndex + 1;
            while (nextTurn == Turn.None)
            {
                // next piece is going to contain switch, time to send switch message
                double lefts = 0;
                double rights = 0;

                foreach (var p in Dashboard.NextPieces(pi))
                {
                    if (p.Turn == Turn.Left) lefts += p.CalculatedLength;
                    if (p.Turn == Turn.Right) rights += p.CalculatedLength;
                    if (p.Switch)
                    {
                        pi = Dashboard.GetPieceIndex(p);
                        break;
                    }
                }

                nextTurn = Math.Abs(rights - lefts) < 0.001 ? Turn.None : rights > lefts ? Turn.Right : Turn.Left;
            }
            Console.WriteLine("Next turn: {0}", nextTurn);

            Message message = null;
            if (nextTurn != Turn.None)
            {
                message = new SwitchLane(nextTurn);
            }
            return message;
        }

        private Message ShouldWeHitTurbo()
        {
            if (Dashboard.TurboAvailable != null)
            {
                if (Dashboard.MyCurrentAndNextPieces().All(m => m.Turn != Turn.None))
                {
                    //its a round-a-buuut!
                    var corner = Dashboard.MyNextPieces().OrderBy(m => Math.Abs(m.Angle)).First();

                    if (corner.Index == Dashboard.MyCurrentPiece().Index)
                    {
                        return new Turbo();
                    }
                }

                if (Math.Abs(Dashboard.MyCurrentPiece().Angle) < 34)
                {
                    if (Dashboard.DistanceToNextTurn() > Dashboard.MyCarInfo.Velocity/2 * Dashboard.TurboAvailable.TurboDurationTicks * Dashboard.TurboAvailable.TurboFactor ) // good enough straight
                    {
                        return new Turbo();
                    }

                    // going too slow, just hit it
                    if (Dashboard.MyCarInfo.Velocity <= 4 && Dashboard.MyCarInfo.Acceleration > 0)
                    {
                        return new Turbo();
                    }
                }
            }

            return null;
        }

        private static double MaintainSpeed(double velocity, double targetSpeed)
        {
            var hysteresis = targetSpeed / 10;

            if (Math.Abs(velocity - targetSpeed) < 0.001)
            {
                return Clamp(targetSpeed / 10.0, 0, 1);
            }

            if (velocity < targetSpeed)
            {
                return Clamp(targetSpeed - velocity + hysteresis, 0.0, 1.0);
            }

            return Clamp(targetSpeed - velocity - hysteresis, 0.0, 1.0);
        }

        public static double Clamp(double d, double min, double max)
        {
            if (d < min) return min;
            if (d > max) return max;
            return d;
        }
    }
}
