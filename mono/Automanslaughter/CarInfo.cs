using System;
using Automanslaughter.Protocol.Incoming;

namespace Automanslaughter
{
    public class CarInfo
    {
        private CarPosition _lastPosition;
        private int _lastGameTick;

        public double Velocity { get; set; }

        public double Acceleration { get; set; }

        public double CarAngleVelocity { get; set; }

        public double CarAngleAcceleration { get; set; }

        public Car Car { get; private set; }

        public CarPosition CarPosition { get; private set; }

        public CarInfo(Car car)
        {
            Car = car;
        }

        public void Update(CarPosition carPosition, int? gameTick)
        {
            if (CarPosition != null)
                _lastPosition = CarPosition;

            CarPosition = carPosition;

            if (gameTick == null)
            {
                _lastGameTick = 0;
                return;
            }

            var timedelta = gameTick.Value - _lastGameTick;
            _lastGameTick = gameTick.Value;

            if (timedelta <= 0)
            {
                Console.WriteLine("Ajan rakenne on muuttunut!");
                return;
            }

            var angleV = (Math.Abs(carPosition.Angle) - Math.Abs(_lastPosition.Angle)) /  timedelta;
            CarAngleAcceleration = (angleV - CarAngleVelocity) / timedelta;
            if (CarAngleVelocity > 0 && angleV < 0)
            {
                Console.WriteLine("Real maximum angle: {0}", Math.Abs(carPosition.Angle));
            }
            CarAngleVelocity = angleV;

            if (carPosition.PiecePosition.PieceIndex != _lastPosition.PiecePosition.PieceIndex)
            {
                // This is inconclusive! For some reason, InPieceDistance can be over 102 for a piece with length of 100.
                // Currently last velocities and accelerations are assumed.
                //  TODO
            }
            else
            {
                var ipdDelta = (carPosition.PiecePosition.InPieceDistance - _lastPosition.PiecePosition.InPieceDistance);
                var v = ipdDelta/timedelta;
                Acceleration = (v - Velocity) / timedelta;
                Velocity = v;
            }
        }
    }
}