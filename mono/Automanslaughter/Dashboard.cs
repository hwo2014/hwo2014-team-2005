using System;
using System.Collections.Generic;
using System.Linq;
using Automanslaughter.Protocol;
using Automanslaughter.Protocol.Incoming;

namespace Automanslaughter
{
    public class Dashboard
    {
        private GameInit _gameInit;
        private readonly string _teamName;
        private bool _inTheDitch;
        public TurboAvailable TurboAvailable { get; set; }

        public bool InTheDitch
        {
            get { return _inTheDitch; }
            set
            {
                if (value)
                    OnCrashed();

                _inTheDitch = value;
            }
        }

        public delegate void CrashEventHandler(object sender, EventArgs args);
        public event CrashEventHandler Crashed;
        private void OnCrashed()
        {
            CrashEventHandler handler = Crashed;
            if (handler != null)
                handler(this, EventArgs.Empty);
        }

        /// <summary>
        /// Key is car ID
        /// </summary>
        private Dictionary<string, CarInfo> CarInfos { get; set; }

        public Dashboard(string teamName)
        {
            CarInfos = new Dictionary<string, CarInfo>();
            InTheDitch = false;
            _teamName = teamName;
        }

        
        public void Initialize(GameInit gameInit)
        {
            _gameInit = gameInit;

            foreach (var car in gameInit.Race.Cars)
            {
                CarInfos[car.Id.Name] = new CarInfo(car);
            }

            for (int index = 0; index < _gameInit.Race.Track.Pieces.Length; index++)
            {
                var piece = _gameInit.Race.Track.Pieces[index];
                piece.Index = index;
            }
        }

        public double DistanceToNextTurn()
        {
            var distanceToNextPiece = _gameInit.Race.Track.Pieces[MyCarInfo.CarPosition.PiecePosition.PieceIndex].CalculatedLength - MyCarInfo.CarPosition.PiecePosition.InPieceDistance;

            var nextPiecesToCorner = MyNextPieces().TakeWhile(m => m.Turn == Turn.None);

            return nextPiecesToCorner.Sum(m => m.CalculatedLength) + distanceToNextPiece;
        }

        public Turn NextTurn(int currentPiece)
        {
            var nextPiece = currentPiece + 1;

            if (nextPiece > _gameInit.Race.Track.Pieces.Length - 1)
                nextPiece = 0;

            var piece = _gameInit.Race.Track.Pieces.ElementAtOrDefault(nextPiece);

            if (piece == null)
                return Turn.None;

            return piece.Turn;
        }

        /// <summary>
        /// This doesn't include the piece the car is on.
        /// </summary>
        public IEnumerable<Piece> NextPieces(int piecePosition)
        {
            return _gameInit.Race.Track.Pieces.Skip(piecePosition + 1).Concat(_gameInit.Race.Track.Pieces.Take(piecePosition + 1));
        }

        public bool CanChangeLane(int currentPieceIndex)
        {
            var piece = _gameInit.Race.Track.Pieces.ElementAtOrDefault(currentPieceIndex);

            return piece != null && piece.Switch;
        }

        public Piece GetPiece(int currentPieceIndex)
        {
            var piece = _gameInit.Race.Track.Pieces.ElementAtOrDefault(currentPieceIndex);
            return piece;
        }

        public void Update(CarPositions carPositions)
        {
            foreach (var carPosition in carPositions.Positions)
            {
                var carInfo = CarInfos[carPosition.Id.Name];
                carInfo.Update(carPosition, carPositions.gameTick);
            }
        }

        // convience stuff
        public CarInfo MyCarInfo
        {
            get { return CarInfos[_teamName]; }
        }

        public IEnumerable<Piece> MyNextPieces()
        {
            return NextPieces(MyCarInfo.CarPosition.PiecePosition.PieceIndex);
        }

        /// <summary>
        /// Gets next pieces. If we are near the end of lap, pieces will be
        /// returned from next the start of the next lap. If we are on final
        /// lap near the end of lap, only remaining pieces from that lap are
        /// returned.
        /// </summary>
        public IEnumerable<Piece> MyNextPieces2()
        {
            var pieces = new List<Piece>();
            var pieceIndexOnLapX = MyCarInfo.CarPosition.PiecePosition.PieceIndex;

            if (_gameInit.Race.RaceSession.Laps == 0)
            {
                return NextPieces(pieceIndexOnLapX);
            }

            for (var lap = MyCarInfo.CarPosition.PiecePosition.Lap; lap < _gameInit.Race.RaceSession.Laps; lap++)
            {
                for (var piece = pieceIndexOnLapX; piece < _gameInit.Race.Track.Pieces.Count(); piece++)
                {
                    pieces.Add(_gameInit.Race.Track.Pieces[piece]);
                }

                pieceIndexOnLapX = 0;
            }
            return pieces;
        }

        public Piece MyCurrentPiece()
        {
            var piece = _gameInit.Race.Track.Pieces.ElementAtOrDefault(MyCarInfo.CarPosition.PiecePosition.PieceIndex);
            return piece;
        }

        public IEnumerable<Piece> MyCurrentAndNextPieces()
        {
            return NextPieces(MyCarInfo.CarPosition.PiecePosition.PieceIndex - 1);
        }

        public double GetCarDistanceFromCenter()
        {
            var currentPiece = MyCarInfo.CarPosition.PiecePosition;

            return _gameInit.Race.Track.Lanes[currentPiece.Lane.StartLaneIndex].DistanceFromCenter;
        }

        public int GetPieceIndex(Piece piece)
        {
            return Array.IndexOf(_gameInit.Race.Track.Pieces, piece);
        }

        public int GetStartIndexOfLongestStreak()
        {
            var startOfLongestStreak = -1;
            var lengthOfLongestStreak = 0.0;

            var currentStreakStart = -1;
            var currentStreakLength = 0.0;

            for (var lap = 0; lap < 2; lap++)
            {
                for (var pieceIndex = 0; pieceIndex < _gameInit.Race.Track.Pieces.Count(); pieceIndex++)
                {
                    var piece = _gameInit.Race.Track.Pieces[pieceIndex];

                    if (piece.Turn != Turn.None)
                    {
                        if (currentStreakLength > lengthOfLongestStreak)
                        {
                            startOfLongestStreak = currentStreakStart;
                            lengthOfLongestStreak = currentStreakLength;
                        }

                        currentStreakStart = -1;
                        currentStreakLength = 0.0;

                        continue;
                    }

                    if (currentStreakStart == -1)
                        currentStreakStart = pieceIndex;

                    currentStreakLength += piece.CalculatedLength;
                }
            }

            return startOfLongestStreak;
        }
    }
}