using System;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using Automanslaughter.Driver;

namespace Automanslaughter
{
    public class Bot
    {
        public static void Main(string[] args)
        {
            // example row for command line arguments: hakkinen.helloworldopen.com 8091 automanslaughter aHiZf/hYQhR2Rw usa 1 LaneWarrior
            var host = "hakkinen.helloworldopen.com";
            var port = 8091;
            var botName = "automanslaughter";
            var botKey = "aHiZf/hYQhR2Rw";
            var trackName = "keimola"; // CI requires to send just join message, so track name must be null if started with 4 params
            var carCount = 1;
            var driverName = string.Empty;

            if (args != null && args.Length > 0)
            {
                host = args[0];
                port = int.Parse(args[1]);
                botName = args[2];
                botKey = args[3];
                trackName = args.ElementAtOrDefault(4);
                int.TryParse(args.ElementAtOrDefault(5), out carCount);
                driverName = args.ElementAtOrDefault(6);
            }


            var driverFactory = new DriverFactory();
            var dashboard = new Dashboard(botName);
            var driver = driverFactory.Create(driverName, botName, botKey, dashboard);

            Console.WriteLine("Connecting to {0}:{1} as {2}/{3}. Track {4} with {5} cars. On driver seat: {6}", host, port, driver.TeamName, driver.Key, trackName, carCount, driver.Name);

            using (var client = new TcpClient(host, port))
            {
                var stream = client.GetStream();
                var reader = new StreamReader(stream);
                var writer = new StreamWriter(stream) {AutoFlush = true};

                var guyInTheYellowVest = new RaceOfficial(reader, writer);

                guyInTheYellowVest.JoinDriverToRace(driver, dashboard, trackName, carCount);

                // blockin' call
                guyInTheYellowVest.GetRaceGoing();
            }
        }
    }
}