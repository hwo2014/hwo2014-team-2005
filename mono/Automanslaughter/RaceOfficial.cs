using System;
using System.IO;
using Automanslaughter.Driver;
using Automanslaughter.Protocol;
using Automanslaughter.Protocol.Incoming;
using Automanslaughter.Protocol.Outgoing;

namespace Automanslaughter
{
    public class RaceOfficial
    {
        private readonly StreamReader _reader;
        private readonly StreamWriter _writer;
        private BaseDriver _driver;
        private Dashboard _dashboard;

        public RaceOfficial(StreamReader reader, StreamWriter writer)
        {
            _reader = reader;
            _writer = writer;
        }

        public void JoinDriverToRace(BaseDriver driver, Dashboard dashboard, string trackName, int carCount)
        {
            _driver = driver;
            _dashboard = dashboard;

            Message joinMessage;
            if (!string.IsNullOrEmpty(trackName))
                joinMessage = new JoinRace{TrackName  = trackName, CarCount = carCount, BotId = new BotId {Key = driver.Key, Name = driver.TeamName}};
            else 
                joinMessage = new Join {Key = driver.Key, Name = driver.TeamName};
            Send(joinMessage);
        }

        public void GetRaceGoing()
        {
            if(_driver == null)
                throw new InvalidOperationException("My driver was not set!");

            bool started = false;
            string line;
            while ((line = _reader.ReadLine()) != null)
            {
                Message msg = Message.FromJson(line);

                if (msg is UnimplementedMessage)
                {
                    Console.WriteLine("Unimplemented message: {0}", line);
                }

                switch (msg.msgType)
                {
                    case CarPositions.MsgType:
                        var start = DateTime.Now;
                        _dashboard.Update(msg as CarPositions);
                        Message msgToSend = started ? _driver.MakeMove(msg as CarPositions) : new Ping();
                        if ((DateTime.Now - start).TotalMilliseconds > 10) Console.WriteLine("WARNING - handling packet took too long: {0}", (DateTime.Now - start).TotalMilliseconds);
                        msgToSend.gameTick = msg.gameTick;
                        Send(msgToSend);

                        if (msgToSend is Turbo)
                            _dashboard.TurboAvailable = null;

                        break;
                    case Join.MsgType:
                        Console.WriteLine("Joined");
                        Send(new Ping());
                        break;
                    case GameInit.MsgType:
                        _dashboard.Initialize(msg as GameInit);
                        Send(new Ping());
                        break;
                    case GameEnd.MsgType:
                        Console.WriteLine("Race ended");
                        started = false;
                        Send(new Ping());
                        break;
                    case GameStart.MsgType:
                        Console.WriteLine("Race starts");
                        started = true;
                        Send(new Throttle {Value = 1.0, gameTick = msg.gameTick});
                        break;
                    case TurboAvailable.MsgType:
                        Console.WriteLine("TURBO AVAILABLE");
                        _dashboard.TurboAvailable = msg as TurboAvailable;
                        Send(new Ping());
                        break;
                    case Crash.MsgType:
                        var crash = msg as Crash;

                        if (crash != null && crash.Name == _dashboard.MyCarInfo.Car.Id.Name)
                        {
                            Console.WriteLine("CRASHED :(( Velocity: {0}, Acceleration: {1}, AngleVelocity: {2}, AngleAcceleration: {3}, Steepness {4}", _dashboard.MyCarInfo.Velocity, _dashboard.MyCarInfo.Acceleration, _dashboard.MyCarInfo.CarAngleVelocity, _dashboard.MyCarInfo.CarAngleAcceleration, _dashboard.GetPiece(_dashboard.MyCarInfo.CarPosition.PiecePosition.PieceIndex).Steepness);
                            _dashboard.InTheDitch = true;
                        }

                        Send(new Ping());
                        break;
                    case Spawn.MsgType:
                        var spawn = msg as Spawn;

                        if (spawn != null && spawn.Name == _dashboard.MyCarInfo.Car.Id.Name)
                        {
                            Console.WriteLine("Spawned");
                            _dashboard.InTheDitch = false;
                        }

                        Send(new Ping());
                        break;
                    case TurboEnd.MsgType:
                        var endMessage = msg as TurboEnd;

                        if (endMessage.Name.Equals(_driver.TeamName))
                        {
                            Console.WriteLine("------------------ END OF TURBO -----------------------");
                        }
                        Send(new Ping());
                        break;
                    default:
                        Send(new Ping());
                        break;
                }
            }
        }

        private void Send(Message msg)
        {
            _writer.WriteLine(msg.ToJson());
        }
    }

}
