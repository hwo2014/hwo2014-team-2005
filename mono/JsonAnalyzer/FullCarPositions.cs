using Automanslaughter.Protocol;
using Automanslaughter.Protocol.Incoming;

namespace JsonAnalyzer
{
    internal class FullCarPositions : Message
    {
        public const string MsgType = "fullCarPositions";

        public override string msgType
        {
            get { return MsgType; }
        }

        public FullCarPosition[] Positions { get; set; }
    }

    internal class FullCarPosition : CarPosition
    {
        private double _angleOffset;

        public Vector2 Coordinates { get; set; }

        public override double Angle
        {
            get { return _angleOffset; }
            set { }
        }

        public double AngleOffset
        {
            get { return _angleOffset; }
            set { _angleOffset = value; }
        }
    }
}