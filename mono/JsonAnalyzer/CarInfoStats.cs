﻿namespace JsonAnalyzer
{
    public class CarInfoStats
    {
        public string Color { get; set; }

        public int GameTick { get; set; }

        public double Velocity { get; set; }

        public double Angle { get; set; }

        public double Steepness { get; set; }
    }
}