﻿using System;
using System.Collections.Generic;
using System.IO;
using Automanslaughter.Protocol;
using Newtonsoft.Json;

namespace JsonAnalyzer
{
    class JsonAnalyzerLauncher
    {
        [STAThread]
        static int Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("USAGE: JsonAnalyzerLauncher path_to_file_or_dir");
                return 1;
            }

            var analyzer = new Analyzer();
            var path = args[0];

            if (File.Exists(path))
            {
                UseFile(path, analyzer);
                analyzer.Save();
                return 0;
            }

            if (Directory.Exists(path))
            {
                UseDir(path, analyzer);
                analyzer.Save();
                return 0;
            }

            Console.WriteLine("Invalid path");
            return 2;
            }

        private static void UseDir(string path, Analyzer analyzer)
        {
            foreach (var file in Directory.GetFiles(path, "*.json"))
            {
                UseFile(file, analyzer);
            }
        }

        private static void UseFile(string path, Analyzer analyzer)
        {
            var text = File.ReadAllLines(path);
            foreach (var s in text)
            {
                var list = JsonConvert.DeserializeObject<List<dynamic>>(s);
                foreach (var o in list)
                {
                    var msg = JsonConvert.DeserializeObject<Message>(o.ToString(), new AnalyzerJsonConverter());
                    analyzer.Analyze(msg);
                }
            }
        }
    }
}
