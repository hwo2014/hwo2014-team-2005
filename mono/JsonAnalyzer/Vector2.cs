namespace JsonAnalyzer
{
    internal class Vector2
    {
        public double X { get; set; }

        public double Y { get; set; }
    }
}