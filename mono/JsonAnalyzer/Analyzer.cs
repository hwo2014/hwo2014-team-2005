﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Automanslaughter;
using Automanslaughter.Protocol.Incoming;
using Message = Automanslaughter.Protocol.Message;

namespace JsonAnalyzer
{
    internal class Analyzer
    {
        public List<CarInfo> CarInfos { get; set; }

        public Track Track { get; set; }

        public List<CarInfoStats> CarInfoStats { get; set; }

        public void Analyze(Message msg)
        {
            switch (msg.msgType)
            {
                case GameInit.MsgType:
                    HandleGameInit(msg as GameInit);
                    break;
                case FullCarPositions.MsgType:
                    HandleFullCarPositions(msg as FullCarPositions);
                    break;
                default:
                    Console.WriteLine("Not analyzed: {0}", msg.msgType);
                    break;
            }
        }

        private void HandleFullCarPositions(FullCarPositions fullCarPositions)
        {
            var gameTick = fullCarPositions.gameTick;
            foreach (var fullCarPosition in fullCarPositions.Positions)
            {
                var carInfo = CarInfos.First(ci => ci.Car.Id.Color == fullCarPosition.Id.Color);
                carInfo.Update(fullCarPosition, gameTick);

                if (gameTick.HasValue)
                    CarInfoStats.Add(new CarInfoStats { Angle = carInfo.CarPosition.Angle, Velocity = carInfo.Velocity, Color = carInfo.Car.Id.Color, GameTick = gameTick.Value, Steepness = Track.Pieces[carInfo.CarPosition.PiecePosition.PieceIndex].Steepness });
            }
        }

        private void HandleGameInit(GameInit gameInit)
        {
            Track = gameInit.Race.Track;
            CarInfos = new List<CarInfo>();
            CarInfoStats = new List<CarInfoStats>();

            foreach (var car in gameInit.Race.Cars)
            {
                CarInfos.Add(new CarInfo(car));
            }
        }

        public void Save()
        {
            var lines = new List<string>();
            foreach (var stat in CarInfoStats.GroupBy(stat => stat.Color))
            {
                lines.Add(string.Format("{1}{0}{2}{0}{3}{0}{4}", ",", "GameTick", "Velocity", "Angle", "Steepness"));
                lines.AddRange(stat.Select(s => string.Format(CultureInfo.InvariantCulture, "{1}{0}{2}{0}{3}{0}{4}", ",", s.GameTick, s.Velocity, s.Angle, s.Steepness)));
                lines.Add("");
            }

            Clipboard.SetText(string.Join(Environment.NewLine, lines));
        }
    }
}
