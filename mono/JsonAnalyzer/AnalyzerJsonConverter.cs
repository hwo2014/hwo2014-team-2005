using System;
using System.Collections.Generic;
using Automanslaughter.Protocol;
using Automanslaughter.Protocol.Incoming;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace JsonAnalyzer
{
    class AnalyzerJsonConverter : MessageJsonConverter
    {
        public override Message Create(Type objectType, JObject jObject)
        {
            var type = (string)jObject.Property("msgType");
            switch (type)
            {
                case FullCarPositions.MsgType:
                    return new FullCarPositions();
                case "carVelocities":
                    return new UnimplementedMessage("carVelocities");
            }
            return base.Create(objectType, jObject);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            // Load JObject from stream
            JObject jObject = JObject.Load(reader);

            // Create target object based on JObject
            var target = Create(objectType, jObject);

            var msgType = (string)jObject.Property("msgType");
            var gameTickNode = jObject.SelectToken("gameTick");

            // Populate the object properties
            var data = jObject.SelectToken("data");
            if (data != null)
            {
                switch (msgType)
                {
                    case "gameStart":
                        return target;
                    case "carPositions":
                        return new CarPositions
                        {
                            Positions = serializer.Deserialize<List<CarPosition>>(data.CreateReader()).ToArray(),
                            gameTick = gameTickNode == null ? null : (int?)gameTickNode.Value<int>()
                        };
                    case "fullCarPositions":
                        return new FullCarPositions
                        {
                            Positions = serializer.Deserialize<List<FullCarPosition>>(data.CreateReader()).ToArray(),
                            gameTick = gameTickNode == null ? null : (int?)gameTickNode.Value<int>()
                        };
                    default:
                        try
                        {
                            serializer.Populate(data.CreateReader(), target);
                        }
                        catch (JsonSerializationException)
                        {
                            Console.WriteLine("Unable to deserialize data for type {0}!", target.msgType);
                        }
                        break;
                }
            }

            return target;
        }
    }
}