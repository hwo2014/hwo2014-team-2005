﻿using Automanslaughter;
using Automanslaughter.Protocol.Incoming;
using NUnit.Framework;

namespace UnitTests
{
    [TestFixture]
    class CarInfoTests
    {
        [Test]
        public void TestCarInfoProperties()
        {
            var car = new Car();
            var ci = new CarInfo(car);
            Assert.That(ci.Car, Is.EqualTo(car));

            var carPosition = new CarPosition { PiecePosition = new PiecePosition { InPieceDistance = 10 } };
            ci.Update(carPosition, null);
            Assert.That(ci.CarPosition, Is.EqualTo(carPosition));
        }

        [Test]
        public void TestVelocityAndAcceleration()
        {
            var ci = new CarInfo(new Car());

            // We have to seed the car info with first position before start of
            // racing. This is also done during actual game.
            ci.Update(new CarPosition {PiecePosition = new PiecePosition { InPieceDistance = 0}}, null);
            Assert.That(ci.Velocity, Is.EqualTo(0));
            Assert.That(ci.Acceleration, Is.EqualTo(0));

            ci.Update(new CarPosition {PiecePosition = new PiecePosition { InPieceDistance = 10 }}, 1);
            Assert.That(ci.Velocity, Is.EqualTo(10));
            Assert.That(ci.Acceleration, Is.EqualTo(10));

            ci.Update(new CarPosition { PiecePosition = new PiecePosition { InPieceDistance = 20 } }, 2);
            Assert.That(ci.Velocity, Is.EqualTo(10));
            Assert.That(ci.Acceleration, Is.EqualTo(0));

            ci.Update(new CarPosition { PiecePosition = new PiecePosition { InPieceDistance = 26 } }, 4);
            Assert.That(ci.Velocity, Is.EqualTo(3));
            Assert.That(ci.Acceleration, Is.EqualTo(-3.5));
        }
    }
}
