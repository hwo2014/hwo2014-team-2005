﻿using System;
using Automanslaughter;
using Automanslaughter.Protocol;
using Automanslaughter.Protocol.Incoming;
using NUnit.Framework;

namespace UnitTests
{
    [TestFixture]
    public class DashboardTests
    {
        [Test]
        public void GetStartIndexOfLongestStreak_Keimola()
        {
            const string line = @"{""msgType"":""gameInit"",""data"":{""race"":{""track"":{""id"":""keimola"",""name"":""Keimola"",""pieces"":[{""length"":100.0},{""length"":100.0},{""length"":100.0},{""length"":100.0,""switch"":true},{""radius"":100,""angle"":45.0},{""radius"":100,""angle"":45.0},{""radius"":100,""angle"":45.0},{""radius"":100,""angle"":45.0},{""radius"":200,""angle"":22.5,""switch"":true},{""length"":100.0},{""length"":100.0},{""radius"":200,""angle"":-22.5},{""length"":100.0},{""length"":100.0,""switch"":true},{""radius"":100,""angle"":-45.0},{""radius"":100,""angle"":-45.0},{""radius"":100,""angle"":-45.0},{""radius"":100,""angle"":-45.0},{""length"":100.0,""switch"":true},{""radius"":100,""angle"":45.0},{""radius"":100,""angle"":45.0},{""radius"":100,""angle"":45.0},{""radius"":100,""angle"":45.0},{""radius"":200,""angle"":22.5},{""radius"":200,""angle"":-22.5},{""length"":100.0,""switch"":true},{""radius"":100,""angle"":45.0},{""radius"":100,""angle"":45.0},{""length"":62.0},{""radius"":100,""angle"":-45.0,""switch"":true},{""radius"":100,""angle"":-45.0},{""radius"":100,""angle"":45.0},{""radius"":100,""angle"":45.0},{""radius"":100,""angle"":45.0},{""radius"":100,""angle"":45.0},{""length"":100.0,""switch"":true},{""length"":100.0},{""length"":100.0},{""length"":100.0},{""length"":90.0}],""lanes"":[{""distanceFromCenter"":-10,""index"":0},{""distanceFromCenter"":10,""index"":1}],""startingPoint"":{""position"":{""x"":-300.0,""y"":-44.0},""angle"":90.0}},""cars"":[{""id"":{""name"":""automanslaughter"",""color"":""red""},""dimensions"":{""length"":40.0,""width"":20.0,""guideFlagPosition"":10.0}}],""raceSession"":{""laps"":3,""maxLapTimeMs"":60000,""quickRace"":true}}},""gameId"":""5a49a2f5-29e0-4d4c-83e4-e955b85d3f2e""}";
            var msg = Message.FromJson(line) as GameInit;

            var dashboard = new Dashboard("jorma");
            dashboard.Initialize(msg);

            var startIndex = dashboard.GetStartIndexOfLongestStreak();

            Assert.That(startIndex, Is.EqualTo(35));
        }

        [Test]
        public void TestShittyMath()
        {
            var cornerEntrySpeed = 3.0;
            var velocity = 6.0;

            var a = cornerEntrySpeed;
            var b = velocity;
            var ticksNeededToBrakeToEntrySpeed = (50*(b-a))/49*b;
            Console.WriteLine("result: {0}", ticksNeededToBrakeToEntrySpeed);
        }
    }
}
