using Automanslaughter;
using Automanslaughter.Driver;
using NUnit.Framework;

namespace UnitTests
{
    [TestFixture]
    public class DriverFactoryTests
    {
        private DriverFactory _factory;

        [SetUp]
        public void Setup()
        {
            _factory = new DriverFactory();
        }

        [Test]
        public void TestCreateMadMax()
        {
            var max = _factory.Create("MadMax", string.Empty, string.Empty, new Dashboard("foo"));
            Assert.That(max.Name, Is.EqualTo("MadMax"));
        }

        [Test]
        public void TestCreateReturnsLaneWarriorForUnknownName()
        {
            var max = _factory.Create("these are not the drivers you are looking for", string.Empty, string.Empty, new Dashboard("foo"));
            Assert.That(max.Name, Is.EqualTo("LaneWarrior"));
        }
    }
}