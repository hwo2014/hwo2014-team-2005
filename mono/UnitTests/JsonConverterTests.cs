﻿using System;
using Automanslaughter.Protocol;
using Automanslaughter.Protocol.Incoming;
using Automanslaughter.Protocol.Outgoing;
using FluentAssertions;
using NUnit.Framework;

namespace UnitTests
{
    [TestFixture]
    public class JsonConverterTests
    {
        [Test]
        public void Join_Serialize()
        {
            const string testJson =
@"
{""msgType"": ""join"", ""data"": {
  ""name"": ""Schumacher"",
  ""key"": ""UEWJBVNHDS""
}}
";
            var message = new Join { Name = "Schumacher", Key = "UEWJBVNHDS" };
            var serialized = message.ToJson();

            Assert.That(Sanitize(serialized), Is.EqualTo(Sanitize(testJson)));
        }

        [Test]
        public void YourCar_Deserialize()
        {
            const string testJson = 
@"
{""msgType"": ""yourCar"", ""data"": {
  ""name"": ""Schumacher"",
  ""color"": ""red""
}}
";
            var deserialized = Deserialize<YourCar>(testJson);

            Assert.That(deserialized.Name, Is.EqualTo("Schumacher"));
            Assert.That(deserialized.Color, Is.EqualTo("red"));
        }

        [Test]
        public void CarPositions_Deserialize()
        {
            const string testJson =
@"
{""msgType"": ""carPositions"", ""data"": [
  {
    ""id"": {
      ""name"": ""Schumacher"",
      ""color"": ""red""
    },
    ""angle"": 0.0,
    ""piecePosition"": {
      ""pieceIndex"": 0,
      ""inPieceDistance"": 0.0,
      ""lane"": {
        ""startLaneIndex"": 0,
        ""endLaneIndex"": 0
      },
      ""lap"": 0
    }
  },
  {
    ""id"": {
      ""name"": ""Rosberg"",
      ""color"": ""blue""
    },
    ""angle"": 45.0,
    ""piecePosition"": {
      ""pieceIndex"": 0,
      ""inPieceDistance"": 20.0,
      ""lane"": {
        ""startLaneIndex"": 1,
        ""endLaneIndex"": 1
      },
      ""lap"": 1
    }
  }
], ""gameId"": ""OIUHGERJWEOI"", ""gameTick"": 0}
";

            var deserialized = Deserialize<CarPositions>(testJson);

            deserialized.ShouldBeEquivalentTo(new CarPositions
            {
                gameTick = 0,
                Positions = new []
                {
                    new CarPosition
                    {
                        Id = new CarId { Name = "Schumacher", Color = "red" },
                        Angle = 0.0,
                        PiecePosition = new PiecePosition
                        {
                            PieceIndex = 0,
                            InPieceDistance = 0.0,
                            Lane = new Lane
                            {
                                StartLaneIndex = 0,
                                EndLaneIndex = 0
                            },
                            Lap = 0
                        }
                    },
                    new CarPosition
                    {
                        Id = new CarId { Name = "Rosberg", Color = "blue" },
                        Angle = 45.0,
                        PiecePosition = new PiecePosition
                        {
                            PieceIndex = 0,
                            InPieceDistance = 20.0,
                            Lane = new Lane
                            {
                                StartLaneIndex = 1,
                                EndLaneIndex = 1
                            },
                            Lap = 1
                        }
                    }
                },
            });

        }

        [Test]
        public void GameInit_Deserialize()
        {
            const string testJson =
                @"
{""msgType"": ""gameInit"", ""data"": {
  ""race"": {
    ""track"": {
      ""id"": ""indianapolis"",
      ""name"": ""Indianapolis"",
      ""pieces"": [
        {
          ""length"": 100.0
        },
        {
          ""length"": 100.0,
          ""switch"": true
        },
        {
          ""radius"": 200,
          ""angle"": 22.5
        }
      ],
      ""lanes"": [
        {
          ""distanceFromCenter"": -20,
          ""index"": 0
        },
        {
          ""distanceFromCenter"": 0,
          ""index"": 1
        },
        {
          ""distanceFromCenter"": 20,
          ""index"": 2
        }
      ],
      ""startingPoint"": {
        ""position"": {
          ""x"": -340.0,
          ""y"": -96.0
        },
        ""angle"": 90.0
      }
    },
    ""cars"": [
      {
        ""id"": {
          ""name"": ""Schumacher"",
          ""color"": ""red""
        },
        ""dimensions"": {
          ""length"": 40.0,
          ""width"": 20.0,
          ""guideFlagPosition"": 10.0
        }
      },
      {
        ""id"": {
          ""name"": ""Rosberg"",
          ""color"": ""blue""
        },
        ""dimensions"": {
          ""length"": 40.0,
          ""width"": 20.0,
          ""guideFlagPosition"": 10.0
        }
      }
    ],
    ""raceSession"": {
      ""laps"": 3,
      ""maxLapTimeMs"": 30000,
      ""quickRace"": true
    }
  }
}}";

            var deserialized = Deserialize<GameInit>(testJson);

            deserialized.ShouldBeEquivalentTo(new GameInit
            {
                Race = new Race
                {
                    Track = new Track
                    {
                        Id = "indianapolis", Name = "Indianapolis",
                        Pieces = new []
                        {
                            new Piece { Length = 100.0 },
                            new Piece { Length = 100.0, Switch = true },
                            new Piece { Radius = 200, Angle = 22.5 }
                        },
                        StartingPoint = new StartingPoint
                        {
                            Position = new Position { X = -340.0, Y = -96.0 },
                            Angle = 90.0
                        },
                        Lanes = new[]
                        {
                            new TrackLane { DistanceFromCenter = -20.0, Index = 0 },
                            new TrackLane { DistanceFromCenter = 0.0, Index = 1 },
                            new TrackLane { DistanceFromCenter = 20.0, Index = 2 }
                        }
                    },
                    Cars = new[]
                    {
                        new Car
                        {
                            Id = new CarId { Name = "Schumacher", Color = "red" },
                            Dimensions = new Dimensions { Length = 40.0, Width = 20.0, GuideFlagPosition = 10.0 }
                        },
                        new Car
                        {
                            Id = new CarId { Name = "Rosberg", Color = "blue" },
                            Dimensions = new Dimensions { Length = 40.0, Width = 20.0, GuideFlagPosition = 10.0 }
                        }
                    },
                    RaceSession = new RaceSession
                    {
                        Laps = 3,
                        MaxLapTimeMs = 30000,
                        QuickRace = true
                    }
                },
            });
        }

        [Test]
        public void GameInit_Deserialize_AlternateRaceSession()
        {
            const string testJson =
                @"
{""msgType"": ""gameInit"", ""data"": {
  ""race"": {
    ""track"": {
      ""id"": ""indianapolis"",
      ""name"": ""Indianapolis"",
      ""pieces"": [
        {
          ""length"": 100.0
        },
        {
          ""length"": 100.0,
          ""switch"": true
        },
        {
          ""radius"": 200,
          ""angle"": 22.5
        }
      ],
      ""lanes"": [
        {
          ""distanceFromCenter"": -20,
          ""index"": 0
        },
        {
          ""distanceFromCenter"": 0,
          ""index"": 1
        },
        {
          ""distanceFromCenter"": 20,
          ""index"": 2
        }
      ],
      ""startingPoint"": {
        ""position"": {
          ""x"": -340.0,
          ""y"": -96.0
        },
        ""angle"": 90.0
      }
    },
    ""cars"": [
      {
        ""id"": {
          ""name"": ""Schumacher"",
          ""color"": ""red""
        },
        ""dimensions"": {
          ""length"": 40.0,
          ""width"": 20.0,
          ""guideFlagPosition"": 10.0
        }
      },
      {
        ""id"": {
          ""name"": ""Rosberg"",
          ""color"": ""blue""
        },
        ""dimensions"": {
          ""length"": 40.0,
          ""width"": 20.0,
          ""guideFlagPosition"": 10.0
        }
      }
    ],
    ""raceSession"": {
      ""durationMs"": 20000
    }
  }
}}";

            var deserialized = Deserialize<GameInit>(testJson);

            deserialized.ShouldBeEquivalentTo(new GameInit
            {
                Race = new Race
                {
                    Track = new Track
                    {
                        Id = "indianapolis",
                        Name = "Indianapolis",
                        Pieces = new[]
                        {
                            new Piece { Length = 100.0 },
                            new Piece { Length = 100.0, Switch = true },
                            new Piece { Radius = 200, Angle = 22.5 }
                        },
                        StartingPoint = new StartingPoint
                        {
                            Position = new Position { X = -340.0, Y = -96.0 },
                            Angle = 90.0
                        },
                        Lanes = new[]
                        {
                            new TrackLane { DistanceFromCenter = -20.0, Index = 0 },
                            new TrackLane { DistanceFromCenter = 0.0, Index = 1 },
                            new TrackLane { DistanceFromCenter = 20.0, Index = 2 }
                        }
                    },
                    Cars = new[]
                    {
                        new Car
                        {
                            Id = new CarId { Name = "Schumacher", Color = "red" },
                            Dimensions = new Dimensions { Length = 40.0, Width = 20.0, GuideFlagPosition = 10.0 }
                        },
                        new Car
                        {
                            Id = new CarId { Name = "Rosberg", Color = "blue" },
                            Dimensions = new Dimensions { Length = 40.0, Width = 20.0, GuideFlagPosition = 10.0 }
                        }
                    },
                    RaceSession = new RaceSession
                    {
                        DurationMs = 20000
                    }
                },
            });
        }

        private static T Deserialize<T>(string json) where T : class
        {
            var deserialized = Message.FromJson(json);
            Assert.That(deserialized, Is.Not.Null);
            Assert.That(deserialized, Is.TypeOf<T>());

            return deserialized as T;
        }

        [Test]
        public void Ping_Serialize()
        {
            const string testJson =
@"
{""msgType"": ""ping""}
";
            var message = new Ping();
            var serialized = message.ToJson();

            Assert.That(Sanitize(serialized), Is.EqualTo(Sanitize(testJson)));
        }

        [Test]
        public void Throttle_Serialize()
        {
            const string testJson =
@"
{""msgType"": ""throttle"", ""data"": 1.0}
";
            var message = new Throttle { Value = 1.0 };
            var serialized = message.ToJson();

            Assert.That(Sanitize(serialized), Is.EqualTo(Sanitize(testJson)));
        }


        [Test]
        public void SwitchLane_Serialize()
        {
            const string testJson =
@"
{""msgType"": ""switchLane"", ""data"": ""Left""}
";
            var message = new SwitchLane(Turn.Left);
            var serialized = message.ToJson();

            Assert.That(Sanitize(serialized), Is.EqualTo(Sanitize(testJson)));
        }

        [Test]
        public void TurboAvailable_Deserialize()
        {
            const string testJson =
@"
{""msgType"": ""turboAvailable"", ""data"": {
  ""turboDurationMilliseconds"": 500.0,
  ""turboDurationTicks"": 30,
  ""turboFactor"": 3.0
}}
";
            var deserialized = Deserialize<TurboAvailable>(testJson);

            Assert.That(deserialized.TurboDurationMilliseconds, Is.EqualTo(500.0));
            Assert.That(deserialized.TurboDurationTicks, Is.EqualTo(30));
            Assert.That(deserialized.TurboFactor, Is.EqualTo(3.0));
        }

        [Test]
        public void Turbo_Sserialize()
        {
            var message = new Turbo();
            var serialized = message.ToJson();

            Assert.That(Sanitize(serialized), Contains.Substring(@"""msgType"":""turbo"""));
            Assert.That(Sanitize(serialized), Contains.Substring(@"""data"":"""));
        }

        [Test]
        public void Crash_Deserialize()
        {
            const string testJson = 
@"
{""msgType"": ""crash"", ""data"": {
  ""name"": ""Rosberg"",
  ""color"": ""blue""
}, ""gameId"": ""OIUHGERJWEOI"", ""gameTick"": 3}
";

            var deserialized = Deserialize<Crash>(testJson);

            Assert.That(deserialized.Name, Is.EqualTo("Rosberg"));
            Assert.That(deserialized.Color, Is.EqualTo("blue"));
        }

        [Test]
        public void Spawn_Deserialize()
        {
            const string testJson =
@"
{""msgType"": ""spawn"", ""data"": {
  ""name"": ""Rosberg"",
  ""color"": ""blue""
}, ""gameId"": ""OIUHGERJWEOI"", ""gameTick"": 3}
";

            var deserialized = Deserialize<Spawn>(testJson);

            Assert.That(deserialized.Name, Is.EqualTo("Rosberg"));
            Assert.That(deserialized.Color, Is.EqualTo("blue"));
        }

        private static string Sanitize(string s)
        {
            return s.Replace(" ", "").Replace(Environment.NewLine, "");
        }
    }
}
